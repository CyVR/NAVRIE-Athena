class TypeTools(object):
    """description of class"""

    def tupleToDict(t):
        d = dict((y, x) for x, y in t)
        return d

    def merge_two_dicts(self, x, **y):
        """Given two dicts, merge them into a new dict as a shallow copy."""
        d = dict(**y)
        z = x.copy()
        z.update(d)
        return z

    def dedupDict(d):
        result = {}
        for key,value in d.items():
            if value not in result.values():
                result[key] = value

        return result