from arango import ArangoClient

from uuid import uuid4

from CyVR.NAVRIE.Athena.Lib.TypeTools import TypeTools

from CyVR.NAVRIE.Athena.DatabaseConnection import DatabaseConnection
from CyVR.NAVRIE.Athena.DatabaseInfo import DatabaseInfo

client = ArangoClient()
dbname = DatabaseConnection.getDBName()

db = client.db(dbname)

client = DatabaseConnection.getClient()
graphName = DatabaseInfo.getGraphName()
graphinst = db.graph(graphName)

current_graph = db.graph(graphName)

class ScratchDicts(object):
    """description of class"""

    sd1 = {}
    sd2 = {}
    sd3 = {}
    sd4 = {}
    sd5 = {}    
    sd6 = {}
    sd7 = {}
    sd8 = {}
    sd9 = {}

    def setSD1(input):
        ScratchDicts.sd1=input

    def getSD1():
        return ScratchDicts.sd1

    def setSD2(input):
        ScratchDicts.sd2=input

    def getSD2():
        return ScratchDicts.sd2

    def setSD3(input):
        ScratchDicts.sd3=input

    def getSD3():
        return ScratchDicts.sd3

    def setSD4(input):
        ScratchDicts.sd4=input

    def getSD4():
        return ScratchDicts.sd4

    def setSD5(input):
        ScratchDicts.sd5=input

    def getSD5():
        return ScratchDicts.sd5

    def setSD6(input):
        ScratchDicts.sd6=input

    def getSD6():
        return ScratchDicts.sd6

    def setSD7(input):
        ScratchDicts.sd7=input

    def getSD7():
        return ScratchDicts.sd7

    def setSD8(input):
        ScratchDicts.sd8=input

    def getSD8():
        return ScratchDicts.sd8

    def setSD9(input):
        ScratchDicts.sd9=input

    def getSD9():
        return ScratchDicts.sd9
