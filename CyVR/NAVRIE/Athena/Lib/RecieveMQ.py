import pika
import time
import json

from CyVR.NAVRIE.Athena.DatabaseInfo import DatabaseInfo

class RecieveMQ(object):
    """
    Call createqueue
    then do a custom callback
    then call consume setup
    """

    def createQueue(prefetch_countdef, callback, queueName):
        
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host = DatabaseInfo.arangodbhost))
        channel = connection.channel()
        channel.queue_declare(queue=queueName, durable=True)
        channel.basic_qos(prefetch_count=prefetch_countdef)
        channel.basic_consume(callback,
                              queue=queueName)
        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()

    def callback(ch, method, properties, msgbody):
        # parse incoming message
        # add incoming to new document
        
        # return json of nmap json
        
        print(" [x] Done")
        ch.basic_ack(delivery_tag = method.delivery_tag)

    def consumeSetup(prefetch_countdef, callback, queueName):

        channel.basic_qos(prefetch_count=prefetch_countdef)
        channel.basic_consume(callback,
                              queue=queueName)

        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()