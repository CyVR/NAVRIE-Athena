from arango import ArangoClient

from uuid import uuid4

from CyVR.NAVRIE.Athena.Lib.TypeTools import TypeTools

from CyVR.NAVRIE.Athena.DatabaseConnection import DatabaseConnection
from CyVR.NAVRIE.Athena.DatabaseInfo import DatabaseInfo

client = ArangoClient()
dbname = DatabaseConnection.getDBName()

db = client.db(dbname)

client = DatabaseConnection.getClient()
graphName = DatabaseInfo.getGraphName()
graphinst = db.graph(graphName)

current_graph = db.graph(graphName)

class GraphDB(object):
    """description of class"""

    def generateUUID():
        uid_str = uuid4().urn
        keykeyvalue = uid_str[9:]
        return keykeyvalue

    def generateUUIDkvp():
        keykeyvalue = generateUUID()
        kkvp = {"_key": keykeyvalue}
        return kkvp
    
    def createEdge(edgeCollection, fromKey, toKey):
        # Retrieve an existing edge collection
        edgecol = graphinst.edge_collection(edgeCollection)

        # Edge collections have a similar interface to standard collections
#        try:
        edgecol.insert({
            '_key': edgeCollection + "_" + fromKey + "->" + toKey,
            '_from': fromKey,
            '_to': toKey
        })
 #       except:
#            print("error creating graph edge")

    def initEdgeCollection(edgeColName, fromColList, toColList):
        try:
            # Create a new edge definition (and a new edge collection)
            graphinst.create_edge_definition(
                name=edgeColName,
                from_collections=fromColList,
                to_collections=toColList
            )
        except:
            # Retrieve an existing vertex collection
            print('Edge collection ' + edgeColName + ' already exists!')

    def initVertexCollection(name):
        # Create a new vertex collection
        try:
            current_vertex_collection = current_graph.create_vertex_collection(name)
            return current_vertex_collection
        except:
            # Retrieve an existing vertex collection
            current_vertex_collection = current_graph.vertex_collection(name)
            return current_vertex_collection
        
    def CreateEntry(self, vertexCollection, **kvps):
        self = str(self)
        uid_str = uuid4().urn
        keykeyvalue = uid_str[9:]
        kkvp = {"_key": keykeyvalue}
        mkvps = TypeTools.merge_two_dicts(kkvp, vertexCollection)

        try:
            current_vertex_collection = current_graph.vertex_collection(self)
            result = current_vertex_collection.insert(mkvps)
            return result['_id']

        except:
            # Retrieve an existing vertex collection
            print('error inserting value to graph')

        return keykeyvalue
        
    def ReturnUUIDForKeyValueInCollection(vertexCollection, key, value):
        vc = vertexCollection
        
        for v in vc.find({key: value}):
            return(v['_key'])
