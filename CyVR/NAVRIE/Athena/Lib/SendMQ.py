import pika
import time
from libnmap.process import NmapProcess
from libnmap.parser import NmapParser
from libnmap.objects import NmapService, NmapReport, NmapReport
from libnmap.reportjson import ReportDecoder, ReportEncoder
import json
from CyVR.NAVRIE.Athena.DatabaseInfo import DatabaseInfo

class SendMQ(object):
    """description of class"""

    def publishMessageOverRabbitMQ(message, routingKey):
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host = DatabaseInfo.arangodbhost))
        channel = connection.channel()
        channel.queue_declare(queue=routingKey, durable=True)
        channel.basic_publish(exchange='',
                              routing_key=routingKey,
                              body=str(message),
                              properties=pika.BasicProperties(
                                delivery_mode = 2, # make message persistent
                              ))
        connection.close()

