from arango import ArangoClient

from uuid import uuid4

from CyVR.NAVRIE.Athena.Lib.TypeTools import TypeTools

from CyVR.NAVRIE.Athena.DatabaseConnection import DatabaseConnection
from CyVR.NAVRIE.Athena.DatabaseInfo import DatabaseInfo

client = ArangoClient()
dbname = DatabaseConnection.getDBName()

db = client.db(dbname)

client = DatabaseConnection.getClient()
graphName = DatabaseInfo.getGraphName()
graphinst = db.graph(graphName)

current_graph = db.graph(graphName)

class MessageQueueUtils(object):

    msgbody = ""

    def getMsgBody():
        return MessageQueueUtils.msgbody

    def setMsgBody(newContents):
        MessageQueueUtils.msgbody = newContents