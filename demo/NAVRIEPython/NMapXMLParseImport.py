import pika
import time
from libnmap.process import NmapProcess
from libnmap.parser import NmapParser
from libnmap.objects import NmapService, NmapReport, NmapHost
from libnmap.reportjson import ReportDecoder, ReportEncoder
import json

queue = 'navrie_athena_recieve_queue'
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host = 'localhost'))
channel = connection.channel()

def publishMessageOverRabbitMQ(message, routingKey):
    channel.queue_declare(queue=routingKey, durable=True)
    channel.basic_publish(exchange='',
                          routing_key=routingKey,
                          body=message,
                          properties=pika.BasicProperties(
                             delivery_mode = 2, # make message persistent
                          ))

nmapXmlFile = "googledns.xml"

nmap_report = NmapParser.parse_fromfile(nmapXmlFile)
for NmapHost in nmap_report.hosts:
    data = {}
    ip4 = NmapHost.ipv4
    ip6 = NmapHost.ipv6
    mac = NmapHost.mac
    services = NmapHost.services
    data['NAVRIE_CollectionType'] = "NAVRIE_NetworkAddress"
    data['ip4Address'] = ip4
    data['ip6Address'] = ip6
    data['macAddress'] = mac
    json_data = json.dumps(data)
    publishMessageOverRabbitMQ(json_data, queue)
    for serv in services:
            data = {}
            ip4 = NmapHost.ipv4
            ip6 = NmapHost.ipv6
            mac = NmapHost.mac
            port = serv.port
            proto = serv.protocol
            state = serv.state
            service = serv.service
            banner = serv.banner
            tunnel = serv.tunnel
            owner = serv.owner
            cpelist = serv.cpelist
            reason = serv.reason
            reason_ip = serv.reason_ip
            reason_ttl = serv.reason_ttl
            data['NAVRIE_CollectionType'] = "NAVRIE_Service"
            data['ip4Address'] = ip4
            data['ip6Address'] = ip6
            data['macAddress'] = mac
            data['serviceName'] = service
            data['banner'] = banner
            data['port'] = port
            data['state'] = state
            data['protocol'] = proto
            data['tunnel'] = tunnel
            data['owner'] = owner
            data['cpelist'] = cpelist
            data['reason'] = reason
            data['reason_ip'] = reason_ip
            data['reason_ttl'] = reason_ttl
            json_data = json.dumps(data)
            publishMessageOverRabbitMQ(json_data, queue)

for NmapHost in nmap_report.hosts:
    data = {}
    lastboot = NmapHost.lastboot
    uptime = NmapHost.uptime
    vendor = NmapHost.vendor
    os_fingerprint = NmapHost.os_fingerprint
    os_fingerprinted = NmapHost.os_fingerprinted
    timestamp = NmapHost.endtime
    data['NAVRIE_CollectionType'] = "NAVRIE_Host"
    data['lastboot'] = lastboot
    data['uptime'] = uptime
    data['vendor'] = vendor
    data['os_fingerprint'] = os_fingerprint
    data['os_fingerprinted'] = os_fingerprinted
    data['timestamp'] = timestamp
    data['ip4Address'] = ip4
    data['ip6Address'] = ip6
    data['macAddress'] = mac
    json_data = json.dumps(data)
    publishMessageOverRabbitMQ(json_data, queue)

connection.close()


