from arango import ArangoClient

client = ArangoClient()
db = client.db('navrietest')
students = db.collection('students')

# Set up some test data to query
db.collection('students').insert_many([
    {'_key': 'Abby', 'age': 22},
    {'_key': 'John', 'age': 18},
    {'_key': 'Mary', 'age': 21}
])

# Execute an AQL query which returns a cursor object
cursor = db.aql.execute(
    'FOR s IN students FILTER s.age < @val RETURN s',
    bind_vars={'val': 19},
    batch_size=1,
    count=True
)

# Retrieve the cursor ID
cursor.id

# Retrieve the documents in the current batch
cursor.batch()

# Check if there are more documents to be fetched
cursor.has_more()

# Retrieve the cursor statistics
cursor.statistics()

# Retrieve any warnings produced from the cursor
cursor.warnings()

# Return the next document in the batch
# If the batch is depleted, fetch the next batch
cursor.next()

# Delete the cursor from the server
cursor.close()


for student in students.find({'_key': 'John'}):
    print(student['_key'], student['age'])

# Retrieve document information
