from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

from ipwhois import IPWhois

import pika
import time
import json

from pprint import pprint

tgt= 'google.com'
lookup = IPWhois(tgt)
results = lookup.lookup_rdap(depth=1)
pprint(results)


wj =json.dumps(results)

pprint(wj)


'''

    _regex = {
        'domain_name':          'Domain Name: *(.+)',
        'registrar':            'Registrar: *(.+)',
        'whois_server':         'Whois Server: *(.+)',
        'referral_url':         'Referral URL: *(.+)',  # http url of whois_server
        'updated_date':         'Updated Date: *(.+)',
        'creation_date':        'Creation Date: *(.+)',
        'expiration_date':      'Expir\w+ Date: *(.+)',
        'name_servers':         'Name Server: *(.+)',  # list of name servers
        'status':               'Status: *(.+)',  # list of statuses
        'emails':               EMAIL_REGEX,  # list of email s
        'dnssec':               'dnssec: *([\S]+)',
        'name':                 'Registrant Name: *(.+)',
        'org':                  'Registrant\s*Organization: *(.+)',
        'address':              'Registrant Street: *(.+)',
        'city':                 'Registrant City: *(.+)',
        'state':                'Registrant State/Province: *(.+)',
        'zipcode':              'Registrant Postal Code: *(.+)',
        'country':              'Registrant Country: *(.+)',
    }

'''
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host = 'localhost'))
channel = connection.channel()

channel.queue_declare(queue='navrie_athena_queue', durable=True)

message = "todo"

channel.basic_publish(exchange='',
                      routing_key='navrie_athena_queue',
                      body=message,
                      properties=pika.BasicProperties(
                         delivery_mode = 2, # make message persistent
                      ))
#print(" [x] Sent %r" % message)

connection.close()

